import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ImportsPage extends StatefulWidget {
  const ImportsPage({Key? key}) : super(key: key);

  @override
  _ImportsPageState createState() => _ImportsPageState();
}

class _ImportsPageState extends State<ImportsPage> {
  late final String deviceDescription;

  @override
  void initState() {
    if(kIsWeb) {
      deviceDescription = 'It is a web browser';
    } else {
      if(Platform.isAndroid) {
        deviceDescription = 'It is an Android device';
      } else if (Platform.isIOS) {
        deviceDescription = 'It is an iOS device';
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Imports Demo')),
      body: Center(
        child: Text(deviceDescription),
      ),
    );
  }
}
